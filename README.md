# PyGame Demo APP
Autor: Thorsten Wagener - 1234567

## Install
Create Virtual Env `python3 -m venv .venv`

Activate Virtual Env `source ./.venv/bin/activate`

`pip install -r requirements.txt`

Run `python src/main.py`

```
your_game/
│
├── assets/          # Store your game assets (images, sounds, etc.) here
│
├── src/             # Source code for your game
│   ├── __init__.py  # Make the directory a Python package
│   ├── main.py      # Entry point of your game
│   ├── game/        # Game-related modules and classes
│   │   ├── __init__.py   # Initialize the Game
│   │   ├── object.py     # Object class
│   │   ├── player.py     # Player class
│   │   ├── enemy.py      # Enemy class
│   │   └── ...
│   ├── screens/     # Screen-related modules
│   │   ├── start_screen.py # Screen class
│   │   └── ...
│   ├── graphics/    # Graphics-related modules
│   │   ├── __init__.py
│   │   ├── renderer.py   # Renderer class
│   │   ├── sprite.py     # Sprite class
│   │   └── ...
│   ├── utils/       # Utility modules
│   │   ├── __init__.py
│   │   ├── helpers.py    # Helper functions
│   │   └── ...
│   └── ...
│
├── tests/           # Unit tests for your code
│
├── .gitignore       # Specify files and directories to be ignored by Git
├── README.md        # Project documentation
├── requirements.txt # List of project dependencies
```