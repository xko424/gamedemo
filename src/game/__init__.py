import pygame
from .player import Player
from .enemy import Enemy
from .physics import PhysicsEngine
from .platform import Platform


class Game:
    def __init__(self, screen):
        self.screen = screen
        # Gruppen erstellen
        self.enemy_sprites = pygame.sprite.Group()
        self.player_sprites = pygame.sprite.Group()
        self.platform_sprites = pygame.sprite.Group()
        self.all_sprites = pygame.sprite.Group()
        self.init()

    def init(self):
        # Objekte erstellen
        self.spawn_objects()
        # Enemies
        self.spawn_enemies()
        # Player
        self.spawn_player()

    def reset_game(self):
        for sprite in self.enemy_sprites:
            sprite.kill()
        self.spawn_player()

    def update(self):
        self.platform_sprites.update()
        self.player_sprites.update(screen=self.screen)
        self.enemy_sprites.update(screen=self.screen)
        self.logic()

    def spawn_enemies(self):
        while len(self.enemy_sprites) < 10:
            enemy = Enemy(x=10, y=100, width=30, height=30)
            self.enemy_sprites.add(enemy)
            self.all_sprites.add(enemy)

    def logic(self):
        if len(self.enemy_sprites) < 10:
            self.spawn_enemies()
        if len(self.player_sprites) < 1:
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {"reason": "game_over"}))
            self.reset_game()
            print("game_over")

    def spawn_player(self):
        player = Player(x=100, y=100, width=50, height=50)
        self.player_sprites.add(player)
        self.all_sprites.add(player)

    def spawn_objects(self):
        self.platform_sprites.add(Platform(x=0, y=self.screen.get_height() - 20, width=self.screen.get_width(), height=20))
        self.platform_sprites.add(Platform(x=40, y=self.screen.get_height() - 200, width=100, height=20))
        self.platform_sprites.add(Platform(x=80, y=self.screen.get_height() - 100, width=100, height=20))
        self.platform_sprites.add(Platform(x=120, y=self.screen.get_height() - 280, width=100, height=20))
        self.platform_sprites.add(Platform(x=320, y=self.screen.get_height() - 320, width=100, height=20))
        self.platform_sprites.add(Platform(x=520, y=self.screen.get_height() - 400, width=100, height=20))

        for platform in self.platform_sprites:
            self.all_sprites.add(platform)

    def handle_events(self, event):
        if len(self.player_sprites) < 1:
            return 'end'

    def render(self, renderer):
        renderer.render_background()
        renderer.render_sprites(self.all_sprites)
        renderer.update_display()
